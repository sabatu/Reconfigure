#include <iostream>
#include <string>
#include "GameObject.h"

using namespace std;




//Getters for GameObject

string GameObject::getDescription() const
{
	return GameObject::description;
}

int GameObject::getLocation() const
{
	return GameObject::location;
}

string GameObject::getName() const
{
	return GameObject::name;
}

bool GameObject::isActive() const
{
	return GameObject::active;
}

bool GameObject::isVisible() const
{
	return GameObject::visible;
}

bool GameObject::isInteractive()
{
	return GameObject::interactive;
}




//Getters for Door

bool Door::isOpen() const
{
	return Door::open;
}

bool Door::isLocked() const
{
	return Door::locked;
}

string Door::getType() const
{
	return Door::type;
}

void Door::openDoor()
{
	if (!this->locked)
	{
		string that = "stuff";

		cout << that << endl;

		this->open = true;
		cout << "The door has been opened.";
		return;
	}
	else
	{
		cout << "The door is locked. Unlock it first.";
		return;
	}
}

void Door::unlockDoor()
{
	this->locked = false;
	cout << "The door has been unlocked.";
}

GameObject::Actions Door::requestInteractions()
{
	GameObject::Actions DoorInteractions;

	//Set descriptions of the interactions in the same order that the function pointers are added
	DoorInteractions.actionDescription.push_back("Open the door.");
	DoorInteractions.actionDescription.push_back("Unlock the door.");

	//Declare and add in the function pointers (mind that the correct order is followed)
	void(Door::*openDoorFunc)(void);
	void(Door::*unlockDoorFunc)(void);

	openDoorFunc = &Door::openDoor;
	unlockDoorFunc = &Door::unlockDoor;

	DoorInteractions.v.push_back((void *(GameObject::*)(void *))openDoorFunc);
	DoorInteractions.v.push_back((void *(GameObject::*)(void *))unlockDoorFunc);
	
	return DoorInteractions;

}



