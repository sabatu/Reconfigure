#include <iostream>
#include <cmath>
#include <vector>
#include <memory>
#include <string>

#include "GameObject.h"
#include "Area.h"
#include "Player.h"




using namespace std;

//typedef vector<unique_ptr<GameObject>>* GameObjects;
typedef vector<GameObject *> GameObjects;


int main()
{   
	

	//Initial definitions
	GameObjects roomObj;
	GameObjects playerItems;

	Door *initialDoor = new Door(0, "firstDoor", "A strong metal door.", true, true, true, false, false, "metal");


	roomObj.push_back(initialDoor);

	Room *initialRoom = new Room("starting room", " You are standing in the middle of someone's office."
		"There is a locked door in front of you. Behind you, there is a computer sitting on a desk. and on each adjoining wall sits a couch. What do you want to do?",
		0, 0, 20, 20, roomObj);

	string John = "john";

	playerItems.push_back(initialDoor);

	Player * activePlayer = new Player(John, playerItems);

	initialRoom->getDescription();

	while (true)
	{
		//get the objects
		vector<GameObject*>::iterator it;
		GameObject * comparison;
		vector<Actions> availableActions;
		vector<Actions>::iterator actionsIT;
		Actions currentAction;
		
		vector<string> listActionsDescription;
		void(*actionFunc[2])() = {};

		int optionsCount = 0;

		for (it = roomObj.begin(); it != roomObj.end(); it++)
		{
			comparison = *it;	


			if (comparison->isInteractive())
			{
				//Get the actions structs
				availableActions.push_back(comparison->requestInteractions);

				//Store the action descriptions and associated function pointers in memory
				
				for (actionsIT = availableActions.begin(); actionsIT != availableActions.end(); actionsIT++)
				{
					currentAction = *actionsIT;

					

					//current action...

					listActionsDescription.push_back(currentAction.actionDescription.pop_back);
					actionFunc[optionsCount] = currentAction.v.pop_back;

				}

			}
		} //<=== end of get objects loop	


		while (true)
		{

			//OUTPUT: Prompt the user to make a selection

			cout << "What would you like to do? Please select one of the following:" << endl;


			//List out the interactable options for each object

			for (int i = 0; i < optionsCount; i++)
			{
				cout << (i + 1) << ".) " << listActionsDescription[i] << endl << endl;

			}

			int selection = -1;

			//have the user make a selection

			cout << "Enter decision here: ";
			cin >> selection;

			//process the input: IMPLEMENT INPUT FILTERING

			actionFunc[selection - 1]();
		}
		
	}

	return 0;

}
