#pragma once
#ifndef PLAYER_H_
#define PLAYER_H_
#include <iostream>
#include <cmath>
#include <vector>
#include <memory>
#include <string>

#include "GameObject.h"


using namespace std;

typedef vector<GameObject *> playerItems;



class Player
{

private:

	string name;
	signed int HP;
	signed int location;
	bool alive;
	playerItems possessions;

public:

	//Constructors		


	Player::Player(string playerName, playerItems InPossession)
		: name(playerName),
		HP(100),
		alive(true),
		possessions(InPossession)
	{}
		
		//Getters
		int getHP() const;
		string getName() const;
		int getLocation() const;



};

#endif