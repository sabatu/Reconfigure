#pragma once
#ifndef AREA_H_
#define AREA_H_

#include <iostream>
#include "GameObject.h"
#include <vector>

using namespace std;

typedef vector<GameObject *> GameObjectVector;



class Area
{
	protected:

		string name;
		string description;
		signed int startX;
		signed int startY;
		signed int endX;
		signed int endY;	


	public:

		Area::Area() {};
		Area::Area(string named, string desc, int initX, int initY, int end_X, int end_Y)
			: name(named),
			description(desc),
			startX(initX),
			startY(initY),
			endX(end_X),
			endY(end_Y)

		{}

		//getters
		string getName() const;
		string getDescription() const;
		signed int getStartX() const;
		signed int getStartY() const;
		signed int getEndX() const;
		signed int getEndY() const;

		virtual signed int getArea(signed int startX, signed int startY, signed int endX, signed int endY) = 0;



};

class Room : public Area
{
	
	protected:
		
		GameObjectVector roomObjects;

	public:

		Room::Room(string named, string desc, signed int initX, signed int initY, signed int end_X, signed int end_Y, GameObjectVector ObjInRoom)
			: Area(named, desc, initX, initY, end_X, end_Y),
			roomObjects(ObjInRoom)

		{}

		signed int getArea(signed int startX, signed int startY, signed int endX, signed int endY);
		GameObjectVector returnRoomObjects();


};

typedef vector<Room> RoomsVector;

class Building : public Area
{
	protected:

		RoomsVector rooms;

	public:

		Building::Building(string named, string desc, signed int initX, signed int initY, signed int end_X, signed int end_Y, RoomsVector roomsInBuilding)
			: Area(named, desc, initX, initY, end_X, end_Y),
			rooms(roomsInBuilding)

		{}

		int getArea(signed int startX, signed int startY, signed int endX, signed int endY);








};






#endif