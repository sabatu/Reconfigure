#pragma once
#ifndef GameObject_H_
#define GameObject_H_

#include <iostream>
#include <vector>





using namespace std;

class GameObject
{

protected:
	signed int location;
	string name;
	string description;
	bool visible;
	bool active;
	bool interactive;

public:

	struct Actions {

		vector<string> actionDescription;
		vector<void *(GameObject::*)(void *)> v;

	};

	GameObject::GameObject() {}
	GameObject::~GameObject() {}	
	GameObject::GameObject(int setLoc, string setName, string setDesc, bool setVis, bool setActive, bool setInteractive)
		: location(setLoc),
		name(setName),
		description(setDesc),
		visible(setVis),
		active(setActive),
		interactive(setInteractive)

	{}

	//getters

	int getLocation() const;
	string getName() const;
	string getDescription() const;
	bool isVisible() const;
	bool isActive() const;
    bool isInteractive();

	virtual Actions requestInteractions() = 0;

};

struct Actions {

	vector<string> actionDescription;
	vector<void *(GameObject::*)(void *)> v;

};

typedef vector<Actions> actionsVector;




class Door : public GameObject
{
	protected:
		bool open;
		bool locked;
		string type;
	

	public:
		Door(int setLoc, string setName, string setDesc, bool setVis, bool setActive, bool setInteractive, bool open, bool locked, string typeOfDoor) 
			: GameObject(setLoc, setName, setDesc, setVis, setActive, setInteractive),			
			open(open),
			locked(locked),
			type(typeOfDoor)	

		{}

		//getters

		bool isOpen() const;
		bool isLocked() const;
		string getType() const;

		//Door specific functions

		void unlockDoor();
		void openDoor();
		Actions requestInteractions();



};





#endif
