#include <iostream>
#include "Area.h"

//getters for abstract class area
typedef vector<GameObject> GameObjectVector;


string Area::getName() const
{
	return Area::name;
}

string Area::getDescription() const
{

	cout << Area::description.c_str << endl;
	return Area::description;
}

signed int Area::getStartX() const
{
	return Area::startX;
}

signed int Area::getStartY() const
{
	return Area::startY;
}

signed int Area::getEndX() const
{
	return Area::endX;
}

signed int Area::getEndY() const
{
	return Area::endY;
}

signed int Area::getArea(signed int startX, signed int startY, signed int endX, signed int endY)
{
	return 0;
}


//getters for Room

signed int Room::getArea(signed int startX, signed int startY, signed int endX, signed int endY)
{
	return ((abs(startX - endX)) * (abs(startY - endY)));
}

GameObjectVector Room::returnRoomObjects()
{
	return Room::roomObjects;
}

//getters for Building

signed int Building::getArea(signed int startX, signed int startY, signed int endX, signed int endY)
{
	return ((abs(startX - endX)) * (abs(startY - endY)));
}




